from nslookup import Nslookup
import sys 

# domain = "gmail.com"



def verify_domain(domain= sys.argv[1]):
    check_domain = domain.split("@")[1]

    dns_query = Nslookup(dns_servers=["8.8.8.8"])
    soa_record = dns_query.soa_lookup(check_domain)

    if len(soa_record.response_full) >= 1:
        return "valid"
    else:
        return "invalid"


if __name__ == "__main__":
    verify_domain()
