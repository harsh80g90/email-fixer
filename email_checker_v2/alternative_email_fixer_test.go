/*
This code is responsible for unit testing of all functions for email_checker code.
*/
package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

var InputCases = []string{
	"xyz@gmal.con",
	"xyzgmail.com",
	"xyz@gmail.co.in",
	"xyz@gmal",
	"xyz@xyz.con",
	"xyz@",
	"xyz@gmail.",
	"xyz@.",
	"xyz@gmail",
}

var OutputCases = []string{
	"xyz@gmail.com",
	"",
	"xyz@gmail.co.in",
	"xyz@gmail.com",
	"xyz@xyz.com",
	"",
	"xyz@gmail.com",
	"",
	"xyz@gmail.com",
}

//SLiceMatch check if two arrays contains same values or not
func SliceMatch(TestStringA, TestStringB []string) bool {
	LenTestStringA := len(TestStringA)
	LenTestStringB := len(TestStringB)

	if LenTestStringA != LenTestStringB {
		return false
	}

	for Item := 0; Item < LenTestStringA; Item++ {
		if TestStringA[Item] != TestStringB[Item] {
			return false
		}
	}

	return true
}

func TestStringInSlice(t *testing.T) {
	var testArray = []string{
		"string1",
		"string2",
		"string3",
	}

	if StringInSlice("string1", testArray) != true {
		t.Errorf("error")
	}
	if StringInSlice("string4", testArray) != false {
		t.Errorf("error")
	}
}

func TestCheckValidation(t *testing.T) {

	if CheckValidation("gmail", "level_one_domain.txt") != true {
		t.Errorf("error")
	}
	if CheckValidation("xyz", "level_one_domain.txt") != false {
		t.Errorf("error")
	}
	if CheckValidation("gmail", "fake_file_domain.txt") != false {
		t.Errorf("error")
	}
}

func TestFindMin(t *testing.T) {

	var InputList = []int{12, 45, 125, 43}

	Maximum := FindMin(InputList)

	if Maximum != 12 {
		t.Errorf("error")
	}
}

func TestMinimunValue(t *testing.T) {

	Minimum := MinimunValue(12, 45, 125, 43)

	if Minimum != 12 {
		t.Errorf("error")
	}
}

func TestMinDistance(t *testing.T) {
	result := MinDistance("gmai", "gmail")
	if result != 1 {
		t.Errorf("error")
	}
}

func TestProcessAndCheck(t *testing.T) {

	result1 := ProcessAndCheck("gmal", "level_one_domain.txt")
	if result1 != "gmail" {
		t.Errorf("error")
	}

	result2 := ProcessAndCheck("con", "level_two_domain.txt")
	if result2 != "com" {
		t.Errorf("error")
	}

	if len(ProcessAndCheck("gmal", "fake_file_domain.txt")) == 0 {
		t.Errorf("error")
	}
}

func TestValidateEmail(t *testing.T) {

	var ResultList []string

	rr := httptest.NewRecorder()

	for Input := 0; Input < len(InputCases); Input++ {
		Result := ValidateEmail(rr, InputCases[Input])
		ResultList = append(ResultList, Result)
	}

	if SliceMatch(ResultList, OutputCases) != true {
		t.Errorf("error")
	}

}

func TestEmailChecker(t *testing.T) {

	req, err := http.NewRequest("POST", "/fix_email", nil)
	if err != nil {
		t.Fatal(err)
	}

	q := req.URL.Query()
	for Input := 0; Input < len(InputCases); Input++ {
		q.Add("email", InputCases[Input])
	}

	req.URL.RawQuery = q.Encode()
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(EmailChecker)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}

func TestPingChecker(t *testing.T) {
	req, err := http.NewRequest("POST", "/ping", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(PingChecker)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}
