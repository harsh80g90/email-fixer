/*
This code is responsible for changing and checking email if the email is not valid
or the email is typed wrong
*/

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	ss "strings"
	"time"
)

// StringInSlice checks string exists in array of string or not
func StringInSlice(a string, list []string) bool {
	for _, item := range list {
		if item == a {
			return true
		}
	}
	return false
}

// CheckValidation check domain exist in txt file or not
func CheckValidation(testEmail string, fileName string) bool {

	var result bool

	data, err := ioutil.ReadFile(fileName)

	// error check
	if err != nil {
		fmt.Println("No File Found")

	}
	// saving the domain name into list so that we can use futher
	validDomain := ss.Split(string(data), "\n")

	for i := 0; i < len(validDomain); i++ {
		result = StringInSlice(testEmail, validDomain)
	}

	return result
}

// FindMin used to find minimum integer in an array
func FindMin(number []int) (min int) {
	//max = number[0]
	if len(number) > 0 {
		min = number[0]
		for _, value := range number {
			if value < min {
				min = value
			}
		}
	}
	return min
}

// MinimunValue is used to get minimum value
func MinimunValue(value int, values ...int) int {
	for _, v := range values {
		if v < value {
			value = v
		}
	}
	return value
}

// MinDistance is used to find minimum changes require to convert string1 to string2
// dynamicValue [i] [j] represents the first i character of w1 to the first j characters of w2
// dynamicValue [i-1] [j] represents the first i-1 character of w1 The steps required to convert to the first j characters of w2
// dynamicValue [i] [j-1] represents the first i-1 character of w1. The steps needed to convert to the first j characters of w2
// dynamicValue [i] [j-1] represents the first i-1 character of w1. The steps required to convert to the first j characters of w2
// When w1 [i] == w2 [j]
// 	dp[i][j] = dp[i-1][j-1]
// When w1 [i]! = W2 [j]
// 	dp[i][j] = min(dp[i-1][j], dp[i][j-1], dp[i-1][j-1]) + 1
func MinDistance(word1 string, word2 string) int {
	dynamicValue := make([][]int, len(word1)+1)
	for i := range dynamicValue {
		dynamicValue[i] = make([]int, len(word2)+1)
		dynamicValue[i][0] = i
	}
	for j := range dynamicValue[0] {
		dynamicValue[0][j] = j
	}
	for i := 1; i < len(dynamicValue); i++ {
		for j := 1; j < len(dynamicValue[0]); j++ {
			offset := 1
			if word1[i-1] == word2[j-1] {
				offset = 0
			}
			dynamicValue[i][j] = MinimunValue(dynamicValue[i-1][j]+1, dynamicValue[i][j-1]+1, dynamicValue[i-1][j-1]+offset)
		}
	}
	return dynamicValue[len(dynamicValue)-1][len(dynamicValue[0])-1]
}

// ProcessAndCheck is used to repair spelling of input domain
func ProcessAndCheck(text string, fileName string) string {

	var finalResult string
	var compareList []int
	var newList []int

	similarityThreshold := 2

	// lower case convertion
	text = ss.ToLower(text)

	// reading the txt file which contains the domain names
	data, err := ioutil.ReadFile(fileName)

	// error check
	if err != nil {
		fmt.Println("Could not able read the file", err)
		return "Could not able read the file"
	}
	// saving the domain name into list so that we can use futher
	validDomain := (ss.Split(string(data), "\n"))

	for i := 0; i < len(validDomain); i++ {
		compareList = append(compareList, MinDistance(validDomain[i], text))
	}

	for i := 0; i < len(compareList); i++ {
		if compareList[i] <= similarityThreshold {
			newList = append(newList, compareList[i])
		}
	}

	if len(newList) > 0 {

		// this will give us the MaxValue after compare
		maxValue := FindMin(newList)

		// this loop is responsible for getting the index
		// value of the MaxValue and assing it into name
		for i := 0; i < len(compareList); i++ {
			if compareList[i] == maxValue {
				finalResult = validDomain[i]
			}
		}
	} else {
		finalResult = text
	}
	return finalResult
}

// ConstructEmail is used to construct valid email
func ConstructEmail(email, levelOne, levelTwo string) string {

	var finalEmail string
	var rest string
	var finalResult string

	emailCount := ss.Count(email, ".")
	emailName := ss.Split(email, "@")[0]

	if emailCount < 2 {
		finalEmail = emailName + "@" + levelOne + "." + levelTwo

	} else {
		rest = ss.Join(ss.Split(email, ".")[2:], ".")
		finalEmail = emailName + "@" + levelOne + "." + levelTwo + "." + rest
	}

	// validationResult validates if domain exist or not
	validationResult := domainValidation(finalEmail)

	if validationResult == "Valid" {
		finalResult = finalEmail
	} else {
		finalResult = email
	}

	return finalResult
}

// domainValidatation is use to validate the domain.
func domainValidation(email string) string {

	var check string

	email = strings.Split(email, "@")[1]

	// check weather the ip of domain exists or not
	ips, err := net.LookupIP(email)

	// this is for error checking
	if err != nil {

		fmt.Println("Could not find IPs for given domain", err)
		check = "InValid"
	}
	// this is for domain validation
	if len(ips) >= 1 {
		check = "Valid"
	}

	return check
}

// ValidateEmail is used to return accurate email
func ValidateEmail(w http.ResponseWriter, email string) string {

	var resultOne string
	var resultTwo string
	var finalDomain string
	var levelTwoDomain string
	var levelOneDomain string
	// var finalResult string

	// first we're checking if email contain (@)
	if ss.Contains(email, "@") {

		// then check if it contains (.)
		if ss.Contains(email, ".") {

			// which insure that we have at least 2 values
			Domain := ss.Split(email, "@")[1]
			levelOneDomain = ss.Split(Domain, ".")[0]
			levelTwoDomain = ss.Split(Domain, ".")[1]

			if len(levelOneDomain) != 0 || len(levelTwoDomain) != 0 {

				if CheckValidation(levelOneDomain, "level_one_domain.txt") {
					resultOne = levelOneDomain
				} else {
					resultOne = ProcessAndCheck(levelOneDomain, "level_one_domain.txt")
				}

				if CheckValidation(levelTwoDomain, "level_two_domain.txt") {
					resultTwo = levelTwoDomain
				} else {
					if len(levelTwoDomain) == 0 {
						resultTwo = "com"
					} else {
						resultTwo = ProcessAndCheck(levelTwoDomain, "level_two_domain.txt")
					}
				}

				// fmt.Println(email)
				// finalResult := ss.ReplaceAll(email, levelOneDomain, resultOne)
				// fmt.Println(resultOne)
				// finalDomain = ss.ReplaceAll(finalResult, levelTwoDomain, resultTwo)
				// fmt.Println(resultTwo)

				finalDomain = ConstructEmail(email, resultOne, resultTwo)

			} else {
				fmt.Fprint(w, "Email is not Valid")
			}
		} else {
			// this logic is to convert xyz@gmail -> xyz@gmail.com
			// this logic executes when levelTwoDomain is empty and "." is not present
			levelOneDomain := ss.Split(email, "@")[1]

			if len(levelOneDomain) != 0 {

				if CheckValidation(levelOneDomain, "level_one_domain.txt") {
					resultOne = levelOneDomain
				} else {
					resultOne = ProcessAndCheck(levelOneDomain, "level_one_domain.txt")
				}

				finalResult := ss.ReplaceAll(email, levelOneDomain, resultOne)

				// ".com" is hardcoded for suggestions
				//finalDomain = finalResult + ".com"
				finalDomain = ConstructEmail(finalResult, resultOne, "com")

			} else {
				// fmt.Fprintf(w, "Email is not Valid")
				fmt.Fprint(w, email)
				fmt.Println("Error occurs in ValidateEmail function (entered email) :- ", email)
			}
		}
	} else {
		// fmt.Fprintf(w, "Email is not Valid")
		fmt.Fprint(w, email)
		fmt.Println("Error occurs in ValidateEmail function (entered email) :- ", email)
	}

	return finalDomain
}

// EmailChecker accepts input as a raw email id and return correct email.
func EmailChecker(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	result := r.Form

	inputEmail := result["email"][0]
	output := ValidateEmail(w, inputEmail)

	fmt.Fprint(w, output)
}

// PingChecker is use for check the ping route
func PingChecker(w http.ResponseWriter, r *http.Request) {
	dt := time.Now()
	fmt.Fprint(w, "status ok, compilation_date:"+dt.Format("01-02-2006 15:04:05"))
}

// HandleRequests handles api request
func HandleRequests() {

	// Ping route
	http.HandleFunc("/ping", PingChecker)
	http.HandleFunc("/v1/ping", PingChecker)

	// This Route for fixing the emails
	http.HandleFunc("/fix_email", EmailChecker)
	http.HandleFunc("/v1/fix_email", EmailChecker)

	err := http.ListenAndServe(":"+os.Getenv("PORT"), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func main() {
	HandleRequests()
}
