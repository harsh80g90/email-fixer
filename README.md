# Email checker

Email checker is an api to check emails if given email is valid or not.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. domain.txt (for domain names)
2. go get -u github.com/paul-mannino/go-fuzzywuzzy

### Running the API

run API

```
source env.sh

go run email_checker.go
```

## testing output

Check for email at http://127.0.0.1:9090/fix_email/

example: 

INPUT:

    email = "someone@gma.cm"

OUTPUT:

    { "someone@gmail.com" }


## Built With

* [HTTP protocol](https://astaxie.gitbooks.io/build-web-application-with-golang/content/en/03.2.html) - The web framework used for GO
