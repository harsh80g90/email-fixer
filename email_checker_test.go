/*
This code is responsible for unit testing of all functions for email_checker code.
*/
package main

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var InputCases = []string{
	"xyz@gma.co",
	"xyz@gil.om",
	"xyz@gmail.cm",
	"xyz@gmail.con",
	"xyz@gmail.co.in",
	"xyz@gmail",
	"xyz.cm",
	"xyz@",
	"xyz@gmail.con.com",
	"",
	"xyx@alo.com",
	"xyz@xyz.com",
}

//SLiceMatch check if two arrays contains same values or not
func SliceMatch(TestStringA, TestStringB []string) bool {
	LenTestStringA := len(TestStringA)
	LenTestStringB := len(TestStringB)

	if LenTestStringA != LenTestStringB {
		return false
	}

	for Item := 0; Item < LenTestStringA; Item++ {
		if TestStringA[Item] != TestStringB[Item] {
			return false
		}
	}

	return true
}

func TestExtractEmail(t *testing.T) {

	var ResultList []string
	var LengthResultList []int

	var OutputCases = []string{
		"gma",
		"gil",
		"gmail",
		"gmail",
		"gmail",
		"gmail",
		"Email is not Valid",
		"Email is not Valid",
		"xyz@gmail.com.com",
		"Email is not Valid",
		"xyx@alo.com",
		"xyz@xyz.com",
	}

	for Input := 0; Input < len(InputCases); Input++ {

		Result, Length := ExtractEmail(InputCases[Input])

		ResultList = append(ResultList, Result)
		LengthResultList = append(LengthResultList, Length)
	}

	if SliceMatch(ResultList, OutputCases) != true {
		t.Errorf("error")
	}
	if len(LengthResultList) != len(OutputCases) {
		t.Errorf("error")
	}

}

func TestDotCom(t *testing.T) {

	var ResultList []string

	var OutputCases = []string{
		"xyz@gma.com",
		"xyz@gil.com",
		"xyz@gmail.com",
		"xyz@gmail.com",
		"xyz@gmail.co.in",
		"xyz@gmail",
		"xyz.cm",
		"Email is not Valid",
		"xyz@gmail.com.com",
		"Email is not Valid",
		"xyx@alo.com",
		"xyz@xyz.com",
	}

	for Input := 0; Input < len(InputCases); Input++ {
		ResultList = append(ResultList, DotCom(InputCases[Input]))
	}

	if SliceMatch(ResultList, OutputCases) != true {
		t.Errorf("error")
	}

}

func TestFindMax(t *testing.T) {

	var InputList = []int{12, 45, 125, 43}

	Maximum := FindMax(InputList)

	if Maximum != 125 {
		t.Errorf("error")
	}

}

func TestProcessAndCheck(t *testing.T) {

	os.Getenv("DOMAINS")

	var ResultList []string

	var OutputCases = []string{
		"xyz@gmail.com",
		"xyz@gmail.com",
		"xyz@gmail.com",
		"xyz@gmail.com",
		"xyz@gmail.co.in",
		"xyz@gmail",
		"xyz.cm",
		"xyz@",
		"xyz@gmail.com.com",
		"Email is not Valid",
		"xyx@alo.com",
		"xyz@gmail.com",
	}

	for Input := 0; Input < len(InputCases); Input++ {
		ResultList = append(ResultList, ProcessAndCheck(InputCases[Input]))
	}

	if SliceMatch(ResultList, OutputCases) != true {
		t.Errorf(" error ")
	}

}

func TestEmailChecker(t *testing.T) {

	req, err := http.NewRequest("POST", "/fix_email", nil)
	if err != nil {
		t.Fatal(err)
	}

	q := req.URL.Query()
	for Input := 0; Input < len(InputCases); Input++ {
		q.Add("email", InputCases[Input])
	}

	req.URL.RawQuery = q.Encode()
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(EmailChecker)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}

func TestPingChecker(t *testing.T) {
	req, err := http.NewRequest("POST", "/ping", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(PingChecker)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}
