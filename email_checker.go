/*
This code is responsible for changing and checking email if the email is not valid
or the email is typed wrong
*/

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	fuzzy "github.com/paul-mannino/go-fuzzywuzzy"
)

// ExtractEmail return the domain (i.e 'gmail','yahoo'.....)
func ExtractEmail(email string) (string, int) {
	var value string
	var length int
	// split on @
	if strings.Contains(email, "@") {
		length = len(strings.Split(email, "@")[1])
		if len(strings.Split(email, "@")) >= 2 {

			splitAt := strings.Split(email, "@")[1]
			// split on (.)
			value = strings.Split(splitAt, ".")[0]

		} else {
			log.Println("Email is not valid")
		}
	} else {
		log.Println("Email is not Valid")
	}

	return value, length
}

// DotCom extract the value before the domain name
func DotCom(email string) string {

	similarityThreshold := 50
	// split on @
	if strings.Contains(email, "@") {
		if (len(strings.Split(email, "@")[1])) >= 2 {

			splitAt := strings.Split(email, "@")[1]

			// getting the gamil value split into (.)
			// this logic check email like xyz@gmail.co.in
			if strings.Contains(splitAt, ".") {

				if len(strings.Split(splitAt, ".")) >= 3 {

					if strings.Contains(splitAt, ".") {
						value := strings.Split(splitAt, ".")[1]

						if value == "co" {
							return email
						} else {
							extentionSimilarity := fuzzy.Ratio(value, "com")

							if extentionSimilarity > similarityThreshold {
								email := strings.ReplaceAll(email, value, "com")
								return email
							}
						}
					} else {
						return email + ".com"
					}
				} else {

					// this logic check email like xyz@gmail.com

					value := strings.Split(splitAt, ".")[1]
					extentionSimilarity := fuzzy.Ratio(value, "com")

					if extentionSimilarity > similarityThreshold {
						email := strings.ReplaceAll(email, value, "com")
						return email
					}
				}
			} else {
				log.Println("Email is not Valid")
			}
		}
	}
	return email
}

// FindMax finds max value
func FindMax(number []int) (max int) {
	max = number[0]
	for _, value := range number {
		if value > max {
			max = value
		}
	}
	return max
}

// ProcessAndCheck is used for process the email and check
// if the giving domain name is valid or not
// if not then we're changing with the right one else do nothing
func ProcessAndCheck(email string) string {

	var name string
	var finalEmail string
	var compareList []int
	var newList []int

	similarityThreshold := 50

	// check for the com
	com := DotCom(email)

	// this is going to give (domain) value
	/*
		input :- himanshu@gmail.com
		output :- gmail
	*/
	domain, domainLength := ExtractEmail(com)
	if domainLength >= 2 {
		// reading the txt file which contains the domain names
		data, err := ioutil.ReadFile(os.Getenv("DOMAINS"))

		// error check
		if err != nil {
			fmt.Println(err)
		}
		// saving the domain name into list so that we can use futher
		validDomain := (strings.Split(string(data), "\n"))

		for i := 0; i < len(validDomain); i++ {
			compareList = append(compareList, fuzzy.Ratio(validDomain[i], domain))
		}

		for i := 0; i < len(compareList); i++ {
			if compareList[i] > similarityThreshold {
				newList = append(newList, compareList[i])
			}
		}

		if len(newList) > 0 {

			// this will give us the MaxValue after compare
			maxValue := FindMax(newList)

			// this loop is responsible for getting the index
			// value of the MaxValue and assing it into name
			for i := 0; i < len(compareList); i++ {
				if compareList[i] == maxValue {
					name = validDomain[i]
				}
			}
			finalEmail = strings.ReplaceAll(com, domain, name)
		} else {
			com := DotCom(email)
			finalEmail = strings.ReplaceAll(com, email, email)
		}
	}
	return finalEmail
}

// EmailChecker accepts input as a raw email id and return correct email.
func EmailChecker(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	result := r.Form

	inputEmail := result["email"][0]
	output := ProcessAndCheck(inputEmail)

	fmt.Fprintf(w, output)
}

// PingChecker is use for check the ping route
func PingChecker(w http.ResponseWriter, r *http.Request) {
	dt := time.Now()
	fmt.Fprintf(w, "status ok, compilation_date:"+dt.Format("01-02-2006 15:04:05"))
}

// HandleRequests handles api request.
func HandleRequests() {

	// Ping route
	http.HandleFunc("/ping", PingChecker)

	// This Route for fixing the emails
	http.HandleFunc("/fix_email", EmailChecker)
	err := http.ListenAndServe(":"+os.Getenv("PORT"), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func main() {
	HandleRequests()
}
