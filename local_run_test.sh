#!/bin/bash
source env.sh
go test --coverprofile=coverage.out ./...
go tool cover -html=coverage.out -o cover.html
google-chrome  cover.html
